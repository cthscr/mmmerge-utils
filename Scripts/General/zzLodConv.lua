--MF = {}
local MF = Merge.Functions
local icons_src_lods = {{"mm8/icons.lod", "mm8icons.txt"}, {"mm7/icons.lod", "mm7icons.txt"}, {"mm6/icons.lod", "mm6icons.txt"}}
--local icons_dst_lods = {{"icons.lod", "icons.txt"}, {"patch.icons.lod", "patch.txt"}}
local icons_dst_lods = {{"icons.lod", "icons.txt"}}
local sprites_src_lods = {{"mm8/sprites.lod", "mm8sprites.txt"}, {"mm7/sprites.lod", "mm7sprites.txt"}, {"mm6/sprites.lod", "mm6sprites.txt"}}
local sprites_dst_lods = {{"sprites.lod", "sprites.txt"}}
local bitmaps_src_lods = {{"mm8/bitmaps.lod", "mm8bitmaps.txt"}, {"mm7/bitmaps.lod", "mm7bitmaps.txt"}, {"mm6/bitmaps.lod", "mm6bitmaps.txt"}}
local bitmaps_dst_lods = {{"bitmaps.lod", "bitmaps.txt"}}
local magic_src_lods = {{"mm8/magicdod.vid", "mm8magic.txt"}, {"mm7/magic7.vid", "mm7magic.txt"}, {"mm6/anims2.vid", "mm6magic.txt"}}
--local magic_src_lods = {{"mm8/magic.lod", "mm8magic.txt"}, {"mm7/magic.lod", "mm7magic.txt"}, {"mm6/magic.lod", "mm6magic.txt"}}
local magic_dst_lods = {{"magicdod.vid", "magic.txt"}}
local might_src_lods = {{"mm8/mightdod.vid", "mm8might.txt"}, {"mm7/might7.vid", "mm7might.txt"}, {"mm6/anims1.vid", "mm6might.txt"}}
--local might_src_lods = {{"mm8/might.lod", "mm8might.txt"}, {"mm7/might.lod", "mm7might.txt"}, {"mm6/might.lod", "mm6might.txt"}}
local might_dst_lods = {{"mightdod.vid", "might.txt"}}
local d_src_lods = {{"mm8/d.lod", "mm8d.txt"}, {"mm7/d.lod", "mm7d.txt"}, {"mm6/d.lod", "mm6d.txt"}}
local d_dst_lods = {{"d.lod", "d.txt"}}
local dir = "Work/"

local special_rules = {}

local function writelodtbl(sums, f, lodtype)
  f:write("local "..lodtype.."_tbl = {\n")
  for k, v in pairs(sums) do
    f:write('  ["'..k..'"] = {\n')
      if v["src"] then
        f:write('    src = {\n')
        for _, entry in pairs(v["src"]) do
          f:write('      {"'..entry[1]..'", "'..entry[2]..'"},\n')
        end
        f:write('    },\n')
      end
      if v["dst"] then
        f:write('    dst = {\n')
        for _, entry in pairs(v["dst"]) do
          f:write('      {"'..entry[1]..'", "'..entry[2]..'"},\n')
        end
        f:write('    }\n')
      end
    f:write('  },\n')
  end
  f:write('}\n')
end

local function mklodtbl(src_lods, dst_lods, lodtype)
  local sums = {}
  for k, v in pairs(src_lods) do
    local src_lod = v[1]
    local fname = v[2]
    local f = io.open(dir..fname, "r")
    if not f then
      print("Could not open file %s", fname)
      return
    end
    local iter = f:lines()
    for line in iter do
      local sum, file = string.sub(line, 1, 32), string.sub(line, 34)
      sums[sum] = sums[sum] or {}
      sums[sum]["src"] = sums[sum]["src"] or {}
      table.insert(sums[sum]["src"], {src_lod, file})
    end
    io.close(f)
  end
  for k, v in pairs(dst_lods) do
    local dst_lod = v[1]
    local fname = v[2]
    local f = io.open(dir..fname, "r")
    if not f then
      print("Could not open file %s", fname)
      return
    end
    local iter = f:lines()
    for line in iter do
      local sum, file = string.sub(line, 1, 32), string.sub(line, 34)
      sums[sum] = sums[sum] or {}
      sums[sum]["dst"] = sums[sum]["dst"] or {}
      table.insert(sums[sum]["dst"], {dst_lod, file})
    end
    io.close(f)
  end

  local f = io.open(dir..lodtype.."_tbl.lua", "w")
  writelodtbl(sums, f, lodtype)
  io.close(f)

  return sums
end
MF.MkLodTbl = mklodtbl

local function mm6move(lod, fn, lodtype)
  if lod == "mm6/" .. lodtype .. ".lod" then
    os.rename(dir .. lodtype.."/"..fn, dir .. "mm6."..lodtype.."/"..fn)
  end
end
local function mm6move3(lods, fn, lodtype)
  local copied, present = false, false
  for k, lod in pairs(lods) do
    if lod[1] == "mm6/" .. lodtype .. ".lod" then
      present = true
      if string.lower(lod[2]) == string.lower(fn) then
        copied = true
      end
    end
  end
  if present then
    --if copied then
      os.rename(dir..lodtype.."/"..fn, dir.."mm6."..lodtype.."/"..fn)
    --else
      --os.rename(dir..lodtype.."/"..fn, dir.."mm6a."..lodtype.."/"..fn)
    --end
  end
  return present
end
local function mm7move(lod, fn, lodtype)
  if lod == "mm7/" .. lodtype .. ".lod" then
    os.rename(dir .. lodtype.."/"..fn, dir.."mm7."..lodtype.."/"..fn)
  end
end
local function mm7move3(lods, fn, lodtype)
  local copied, present = false, false
  for k, lod in pairs(lods) do
    if lod[1] == "mm7/" .. lodtype .. ".lod" then
      present = true
      if string.lower(lod[2]) == string.lower(fn) then
        copied = true
      end
    end
  end
  if present then
    --if copied then
      os.rename(dir..lodtype.."/"..fn, dir.."mm7."..lodtype.."/"..fn)
    --else
      --os.rename(dir..lodtype.."/"..fn, dir.."mm7a."..lodtype.."/"..fn)
    --end
  end
  return present
end
local function mm8move1(lod, fn, lodtype)
  if lod == "mm8/" .. lodtype .. ".lod" then
    os.rename(dir .. lodtype.."/"..fn, dir.."mm8."..lodtype.."/"..fn)
  end
end
local function mm8move2(lod, fn, lodtype)
  if lod == "mm8/" .. lodtype .. ".lod" then
    os.rename(dir .. lodtype.."/"..fn, dir.."mm8a."..lodtype.."/"..fn)
  end
end
local function mm8move3(lods, fn, lodtype)
  local copied, present = false, false
  for k, lod in pairs(lods) do
    if lod[1] == "mm8/" .. lodtype .. ".lod" then
      present = true
      if string.lower(lod[2]) == string.lower(fn) then
        copied = true
      end
    end
  end
  if present then
    if copied then
      os.rename(dir..lodtype.."/"..fn, dir.."mm8."..lodtype.."/"..fn)
    else
      os.rename(dir..lodtype.."/"..fn, dir.."mm8a."..lodtype.."/"..fn)
    end
  end
  return present
end
local function mm8move4(src, dst, lodtype, do_move)
  if #src ~= #dst then
    return false
  end
  local t = {}
  for _, s in pairs(src) do
    if s[1] ~= "mm8/" .. lodtype .. ".lod" then
      return false
    end
    local present = false
    for __, d in pairs(dst) do
      if string.lower(s[2]) == string.lower(d[2]) then
        table.insert(t, {fn = d[2]})
        present = true
      end
    end
    if not present then
      return false
    end
  end
  if do_move then
    for _, v in pairs(t) do
      os.rename(dir .. lodtype .. "/" .. v.fn, dir .. "mm8." .. lodtype .. "/" .. v.fn)
    end
  end
  return true
end
local function mm67move4(src, dst, lodtype, ver, do_move)
  local t = {}
  for _, s in pairs(src) do
    if s[1] ~= "mm" .. ver .. "/" .. lodtype .. ".lod" then
      return false
    end
    local present = false
    for __, d in pairs(dst) do
      if string.lower(s[2]) == string.lower(d[2]) then
        table.insert(t, {fn = d[2]})
        present = true
      end
    end
    if not present then
      return false
    end
  end
  if do_move then
    for _, v in pairs(t) do
      os.rename(dir .. lodtype .. "/" .. v.fn, dir .. "mm" .. ver .. "." .. lodtype .. "/" .. v.fn)
    end
  end
  return true
end
local function mmmove1(lod, fn, lodtype)
  mm8move1(lod, fn, lodtype)
  mm7move(lod, fn, lodtype)
  mm6move(lod, fn, lodtype)
end
local function mmmove2(lod, fn, lodtype)
  mm8move2(lod, fn, lodtype)
  mm7move(lod, fn, lodtype)
  mm6move(lod, fn, lodtype)
end
local function mmmove3(lods, fn, lodtype)
  return mm8move3(lods, fn, lodtype) or mm7move3(lods, fn, lodtype) or mm6move3(lods, fn, lodtype)
end
-- Try many_copied
local function mmmove4(src, dst, lodtype, do_move)
  if #src ~= #dst then
    return false
  end
  local has6, has7, has8 = 0, 0, 0
  for _, s in pairs(src) do
    if s[1] == "mm8/" .. lodtype .. ".lod" then
      has8 = 1
    elseif s[1] == "mm7/" .. lodtype .. ".lod" then
      has7 = 1
    elseif s[1] == "mm6/" .. lodtype .. ".lod" then
      has6 = 1
    else
      return false
    end
  end
  if has8 + has7 + has6 > 1 then
    return false
  end
  if has8 == 1 then
    return mm8move4(src, dst, lodtype, do_move)
  elseif has7 == 1 then
    return mm67move4(src, dst, lodtype, 7, do_move)
  elseif has6 == 1 then
    return mm67move4(src, dst, lodtype, 6, do_move)
  end
  return false
end
-- Try many2many
local function mmmove5(src, dst, lodtype, do_move)
  if #src ~= #dst then
    return false
  end
  local cnt6, cnt7, cnt8 = 0, 0, 0
  local src6, src7, src8, dst1 = {}, {}, {}, {}
  local t6, t7, t8 = {}, {}, {}
  for _, s in pairs(src) do
    if s[1] == "mm8/" .. lodtype .. ".lod" then
      cnt8 = cnt8 + 1
      table.insert(src8, {s[1], s[2]})
    elseif s[1] == "mm7/" .. lodtype .. ".lod" then
      cnt7 = cnt7 + 1
      table.insert(src7, {s[1], s[2]})
    elseif s[1] == "mm6/" .. lodtype .. ".lod" then
      cnt6 = cnt6 + 1
      table.insert(src6, {s[1], s[2]})
    else
      return false
    end
  end
  for _, d in pairs(dst) do
    table.insert(dst1, {d[1], d[2]})
  end

  if cnt8 > 0 then
    for _, s in pairs(src8) do
      local present = false
      for k, d in pairs(dst1) do
        if string.lower(s[2]) == string.lower(d[2]) then
          table.insert(t8, {fn = d[2]})
          dst1[k] = nil
          present = true
          break
        end
      end
      if not present then
        return false
      end
    end
  end
  if cnt7 > 0 then
    for _, s in pairs(src7) do
      local present = false
      for k, d in pairs(dst1) do
        if string.lower(s[2]) == string.lower(d[2]) then
          table.insert(t7, {fn = d[2]})
          dst1[k] = nil
          present = true
          break
        end
      end
      if not present then
        for k, d in pairs(dst1) do
          if "7" .. string.lower(s[2]) == string.lower(d[2]) then
            table.insert(t7, {fn = d[2]})
            dst1[k] = nil
            present = true
            break
          end
        end
        if not present then
          return false
        end
      end
    end
  end
  if cnt6 > 0 then
    for _, s in pairs(src6) do
      local present = false
      for k, d in pairs(dst1) do
        if string.lower(s[2]) == string.lower(d[2]) then
          table.insert(t6, {fn = d[2]})
          dst1[k] = nil
          present = true
          break
        end
      end
      if not present then
        for k, d in pairs(dst1) do
          if "6" .. string.lower(s[2]) == string.lower(d[2]) then
            table.insert(t6, {fn = d[2]})
            dst1[k] = nil
            present = true
            break
          end
        end
        if not present then
          return false
        end
      end
    end
  end
  if do_move then
    for _, v in pairs(t8) do
      os.rename(dir .. lodtype .. "/" .. v.fn, dir .. "mm8." .. lodtype .. "/" .. v.fn)
    end
    for _, v in pairs(t7) do
      os.rename(dir .. lodtype .. "/" .. v.fn, dir .. "mm7." .. lodtype .. "/" .. v.fn)
    end
    for _, v in pairs(t6) do
      os.rename(dir .. lodtype .. "/" .. v.fn, dir .. "mm6." .. lodtype .. "/" .. v.fn)
    end
  end

  return true
end
-- Multiplied
local function mmmove6(src, dst, lodtype)
  if src[1] == "mm8/" .. lodtype .. ".lod" then
    for _, d in pairs(dst) do
      if string.lower(src[2]) == string.lower(d[2]) then
        mm8move1(src[1], d[2], lodtype)
      else
        mm8move2(src[1], d[2], lodtype)
      end
    end
  elseif src[1] == "mm7/" .. lodtype .. ".lod" then
    for _, d in pairs(dst) do
      mm7move(src[1], d[2], lodtype)
    end
  elseif src[1] == "mm6/" .. lodtype .. ".lod" then
    for _, d in pairs(dst) do
      mm6move(src[1], d[2], lodtype)
    end
  else
    return false
  end
end
-- Undecided (try to copy mm8 files)
local function mmmove7(src, dst, lodtype)
  for _, s in pairs(src) do
    if s[1] == "mm8/" .. lodtype .. ".lod" then
      for __, d in pairs(dst) do
        if string.lower(s[2]) == string.lower(d[2]) then
          mm8move1(s[1], d[2], lodtype)
          break
        end
      end
    end
  end
end

local function processlodtbl(sums, lodtype, do_rename)
  local ignored, added, multiplied, undecided, mult_ignored, many2one = {}, {}, {}, {}, {}, {}
  local copied, renamed, many_copied, many2many = {}, {}, {}, {}
  local copied_cnt, renamed_cnt, ignored_cnt, added_cnt, multiplied_cnt = 0, 0, 0, 0, 0
  local mult_ignored_cnt, undecided_cnt, many2one_cnt, many_copied_cnt = 0, 0, 0, 0
  local many2many_cnt, total = 0, 0
  for k, v in pairs(sums) do
    total = total + 1
    local src_size, dst_size = v.src and #v.src or 0, v.dst and #v.dst or 0
    if src_size == 1 and dst_size == 1 then
      if string.lower(v.src[1][2]) == string.lower(v.dst[1][2]) then
        copied_cnt = copied_cnt + 1
        copied[k] = v
        if do_rename then mmmove1(v.src[1][1], v.dst[1][2], lodtype) end
      else
        renamed_cnt = renamed_cnt + 1
        renamed[k] = v
        if do_rename then mmmove2(v.src[1][1], v.dst[1][2], lodtype) end
      end
    elseif src_size == 1 and dst_size == 0 then
      ignored_cnt = ignored_cnt + 1
      ignored[k] = v
    elseif src_size == 1 then
      multiplied_cnt = multiplied_cnt + 1
      multiplied[k] = v
      if do_rename then mmmove6(v.src[1], v.dst, lodtype) end
    elseif src_size == 0 then
      added_cnt = added_cnt + 1
      added[k] = v
    elseif dst_size == 0 then
      mult_ignored_cnt = mult_ignored_cnt + 1
      mult_ignored[k] = v
    elseif dst_size == 1 then
      many2one_cnt = many2one_cnt + 1
      many2one[k] = v
      if do_rename then mmmove3(v.src, v.dst[1][2], lodtype) end
    else
      if mmmove4(v.src, v.dst, lodtype, do_rename) then
        many_copied_cnt = many_copied_cnt + 1
        many_copied[k] = v
      elseif mmmove5(v.src, v.dst, lodtype, do_rename) then
        many2many_cnt = many2many_cnt + 1
        many2many[k] = v
      else
        undecided_cnt = undecided_cnt + 1
        undecided[k] = v
        if do_rename then mmmove7(v.src, v.dst, lodtype) end
      end
    end
  end
  local f = io.open(dir..lodtype.."_ignored_tbl.lua", "w")
  writelodtbl(ignored, f, lodtype.."_ignored")
  io.close(f)
  f = io.open(dir..lodtype.."_added_tbl.lua", "w")
  writelodtbl(added, f, lodtype.."_added")
  io.close(f)
  f = io.open(dir..lodtype.."_multiplied_tbl.lua", "w")
  writelodtbl(multiplied, f, lodtype.."_multiplied")
  io.close(f)
  f = io.open(dir..lodtype.."_undecided_tbl.lua", "w")
  writelodtbl(undecided, f, lodtype.."_undecided")
  io.close(f)
  f = io.open(dir..lodtype.."_mult_ignored_tbl.lua", "w")
  writelodtbl(mult_ignored, f, lodtype.."_mult_ignored")
  io.close(f)
  f = io.open(dir..lodtype.."_many2one_tbl.lua", "w")
  writelodtbl(many2one, f, lodtype.."_many2one")
  io.close(f)
  f = io.open(dir..lodtype.."_copied_tbl.lua", "w")
  writelodtbl(copied, f, lodtype.."_copied")
  io.close(f)
  f = io.open(dir..lodtype.."_renamed_tbl.lua", "w")
  writelodtbl(renamed, f, lodtype.."_renamed")
  io.close(f)
  f = io.open(dir..lodtype.."_many_copied_tbl.lua", "w")
  writelodtbl(many_copied, f, lodtype.."_many_copied")
  io.close(f)
  f = io.open(dir..lodtype.."_many2many_tbl.lua", "w")
  writelodtbl(many2many, f, lodtype.."_many2many")
  io.close(f)

  print(string.format("Copied %d; renamed %d; ignored %d; added %d; multiplied %d; mult_ignored %d; undecided %d; many2one %d; many_copied %d; many2many %d. Total %d.",
    copied_cnt, renamed_cnt, ignored_cnt, added_cnt, multiplied_cnt, mult_ignored_cnt, undecided_cnt, many2one_cnt, many_copied_cnt, many2many_cnt, total))
end
MF.ProcessLodTbl = processlodtbl

local function proc_icons_lod_tbl(do_rename)
  local icons_tbl = mklodtbl(icons_src_lods, icons_dst_lods, "icons")
  processlodtbl(icons_tbl, "icons", do_rename)
end
MF.ProcIconsLodTbl = proc_icons_lod_tbl
local function proc_sprites_lod_tbl(do_rename)
  local sprites_tbl = mklodtbl(sprites_src_lods, sprites_dst_lods, "sprites")
  processlodtbl(sprites_tbl, "sprites", do_rename)
end
MF.ProcSpritesLodTbl = proc_sprites_lod_tbl
local function proc_bitmaps_lod_tbl(do_rename)
  local bitmaps_tbl = mklodtbl(bitmaps_src_lods, bitmaps_dst_lods, "bitmaps")
  processlodtbl(bitmaps_tbl, "bitmaps", do_rename)
end
MF.ProcBitmapsLodTbl = proc_bitmaps_lod_tbl
local function proc_magiclod_tbl(do_rename)
  local magic_tbl = mklodtbl(magic_src_lods, magic_dst_lods, "magic")
  processlodtbl(magic_tbl, "magic", do_rename)
end
MF.ProcMagicLodTbl = proc_magiclod_tbl
local function proc_mightlod_tbl(do_rename)
  local might_tbl = mklodtbl(might_src_lods, might_dst_lods, "might")
  processlodtbl(might_tbl, "might", do_rename)
end
MF.ProcMightLodTbl = proc_mightlod_tbl
local function proc_dlod_tbl(do_rename)
  local d_tbl = mklodtbl(d_src_lods, d_dst_lods, "d")
  processlodtbl(d_tbl, "d", do_rename)
end
MF.ProcDLodTbl = proc_dlod_tbl

special_rules.bitmaps = {
  ["43dbe29defe0df5cbf9c45842bdbc6c3"] = {
    ["hwtrdrxsw.bmp"] = "mm8",
    ["wtrdrxsw.bmp"] = "mm8"
  },
  ["47e596f0ddc521cc5345cfed1576ff5f"] = {
    ["t22b04a.bmp"] = "mm8",
    ["t22b04b1.bmp"] = "mm8"
  },
  ["68c3dda47e66d4429e823f827d1ecf53"] = {
    ["gkp2-bs1.bmp"] = "mm8",
    ["gkp2-bs2.bmp"] = "mm8"
  },
  ["8212951b8d68ffde5746e08a0c1146d3"] = {
    ["hwtrdrn.bmp"] = "mm8",
    ["wtrdrn.bmp"] = "mm8"
  },
  ["cf5fbc88764f95f413f19659438202d0"] = {
    ["wstdrtnw.bmp"] = "mm8"
  },
}
