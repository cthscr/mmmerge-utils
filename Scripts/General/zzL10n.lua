local MF = Merge.Functions
local strlen, strsub = string.len, string.sub

local src_txt_dir_default = "mmmerge.T.lod/"

local po_dir_default = "po/"
local pot_file = "mmmerge.pot"

local context = {
  [1] = "autonote",
  [2] = "award",
  [3] = "bonus-desc",
  [4] = "bonus-name",
  [5] = "bonus-stat",
  [6] = "class-desc",
  [7] = "class-name",
  [8] = "history-title",
  [9] = "history-text",
  [10] = "house",
  [11] = "item-desc",
  [12] = "item-name",
  [13] = "item-non-identified-name",
  [14] = "map-name",
  [15] = "merchant-buy",
  [16] = "merchant-identify",
  [17] = "merchant-repair",
  [18] = "merchant-sell",
  [19] = "message-scroll",
  [20] = "monster",
  [21] = "npc-greet",
  [22] = "npc-name",
  [23] = "npc-text",
  [24] = "npc-topic",
  [25] = "npcnews-text",
  [26] = "npcnews-topic",
  [27] = "owner-name",
  [28] = "owner-title",
  [29] = "pc-biography",
  [30] = "pc-name",
  [31] = "quest",
  [32] = "skill-desc",
  [33] = "skill-expert",
  [34] = "skill-gm",
  [35] = "skill-master",
  [36] = "skill-name",
  [37] = "skill-normal",
  [38] = "spell-desc",
  [39] = "spell-expert",
  [40] = "spell-gm",
  [41] = "spell-master",
  [42] = "spell-name",
  [43] = "spell-normal",
  [44] = "spell-short",
  [45] = "stat-desc",
  [46] = "stat-name",
  [47] = "str-map",
  [48] = "transition",
  [49] = "owner-title-male",
  [50] = "owner-title-female",
  [51] = "launcher-text",
  [52] = "monster-male",
  [53] = "monster-female",
}

local txts = {
  {"2DEvents.txt", 3, {[6] = {10}, [7] = {27}, [8] = {28}}},
  {"Autonote.txt", 2, {[2] = {1}}},
  {"Awards.txt", 2, {[2] = {2}}},
  {"class.txt", 2, {[1] = {7}, [2] = {6}}},
  {"Global.TXT", 3, {[2] = {0}}},
  {"history.txt", 2, {[2] = {9}, [4] = {8}}},
  {"SPCITEMS.TXT", 5, {[1] = {3}, [2] = {4}}, 1},
  {"STDITEMS.TXT", 5, {[1] = {5}, [2] = {4}}, 1},
  {"ITEMS.txt", 4, {[3] = {12}, [11] = {13}, [13] = {4}, [17] = {11}}},
  {"MapStats.txt", 4, {[2]= {14}}},
  {"MERCHANT.TXT", 2, {[2] = {15}, [3] = {18}, [4] = {17}, [5] = {16}}},
  {"MM7history.txt", 2, {[2] = {9}, [4] = {8}}},
  {"MONSTERS.txt", 5, {[2] = {20}}},
  {"NPCData.txt", 3, {[2] = {22}}},
  {"NPCGreet.txt", 2, {[2] = {21}, [3] = {21}}},
  {"NPCNews.txt", 3, {[2] = {25}, [3] = {26, 102}}},
  {"NPCText.txt", 2, {[2] = {23}}},
  {"NPCTopic.txt", 2, {[2] = {24}}},
  {"Placemon.txt", 2, {[2] = {22}}},
  {"Quests.txt", 2, {[2] = {31}}},
  {"roster.txt", 3, {[2] = {30}, [124] = {29}}},
  {"scroll.txt", 2, {[2] = {19}}},
  {"Skilldes.txt", 2, {[1] = {36}, [2] = {32}, [3] = {37}, [4] = {33}, [5] = {35}, [6] = {34}}},
  {"Spells.txt", 3, {[3] = {42}, [5] = {44}, [6] = {38}, [7] = {43}, [8] = {39}, [9] = {41}, [10] = {40}}},
  {"stats.txt", 2, {[1] = {46}, [2] = {45}}},
  {"Trans.txt", 2, {[2] = {48}}},
  {"Launcher.txt", 4, {[2] = {51}}},
  {"OUT01.STR", 1, {[1] = {47}}},
  {"OUT02.STR", 1, {[1] = {47}}},
  {"OUT03.STR", 1, {[1] = {47}}},
  {"OUT04.STR", 1, {[1] = {47}}},
  {"OUT05.STR", 1, {[1] = {47}}},
  {"OUT06.STR", 1, {[1] = {47}}},
  {"OUT07.STR", 1, {[1] = {47}}},
  {"OUT08.STR", 1, {[1] = {47}}},
  {"ElemA.STR", 1, {[1] = {47}}},
  {"ElemE.STR", 1, {[1] = {47}}},
  {"ElemF.STR", 1, {[1] = {47}}},
  {"ElemW.STR", 1, {[1] = {47}}},
  {"Out13.STR", 1, {[1] = {47}}},
  {"Out15.STR", 1, {[1] = {47}}},
  {"PbP.STR", 1, {[1] = {47}}},
  {"D05.STR", 1, {[1] = {47}}},
  {"D06.STR", 1, {[1] = {47}}},
  {"D07.STR", 1, {[1] = {47}}},
  {"D08.STR", 1, {[1] = {47}}},
  {"D09.STR", 1, {[1] = {47}}},
  {"D10.STR", 1, {[1] = {47}}},
  {"D11.STR", 1, {[1] = {47}}},
  {"D12.STR", 1, {[1] = {47}}},
  {"D13.STR", 1, {[1] = {47}}},
  {"D14.STR", 1, {[1] = {47}}},
  {"D15.STR", 1, {[1] = {47}}},
  {"D16.STR", 1, {[1] = {47}}},
  {"D17.STR", 1, {[1] = {47}}},
  {"D18.STR", 1, {[1] = {47}}},
  {"D19.STR", 1, {[1] = {47}}},
  {"D20.STR", 1, {[1] = {47}}},
  {"D21.STR", 1, {[1] = {47}}},
  {"D22.STR", 1, {[1] = {47}}},
  {"D23.STR", 1, {[1] = {47}}},
  {"D24.STR", 1, {[1] = {47}}},
  {"D25.STR", 1, {[1] = {47}}},
  {"D26.STR", 1, {[1] = {47}}},
  {"D27.STR", 1, {[1] = {47}}},
  {"D28.STR", 1, {[1] = {47}}},
  {"D29.STR", 1, {[1] = {47}}},
  {"D30.STR", 1, {[1] = {47}}},
  {"D31.STR", 1, {[1] = {47}}},
  {"D32.STR", 1, {[1] = {47}}},
  {"D33.STR", 1, {[1] = {47}}},
  {"D34.STR", 1, {[1] = {47}}},
  {"d35.STR", 1, {[1] = {47}}},
  {"D36.STR", 1, {[1] = {47}}},
  {"D37.STR", 1, {[1] = {47}}},
  {"D38.STR", 1, {[1] = {47}}},
  {"D39.STR", 1, {[1] = {47}}},
  {"D40.STR", 1, {[1] = {47}}},
  {"D41.STR", 1, {[1] = {47}}},
  {"d42.str", 1, {[1] = {47}}},
  {"d43.str", 1, {[1] = {47}}},
  {"d44.str", 1, {[1] = {47}}},
  {"d45.str", 1, {[1] = {47}}},
  {"d46.str", 1, {[1] = {47}}},
  {"D47.STR", 1, {[1] = {47}}},
  {"D48.STR", 1, {[1] = {47}}},
  {"D49.STR", 1, {[1] = {47}}},
  {"d50.STR", 1, {[1] = {47}}},
  {"7out01.str", 1, {[1] = {47}}},
  {"7out02.STR", 1, {[1] = {47}}},
  {"7out03.STR", 1, {[1] = {47}}},
  {"7out04.STR", 1, {[1] = {47}}},
  {"7out05.STR", 1, {[1] = {47}}},
  {"7out06.STR", 1, {[1] = {47}}},
  {"out09.STR", 1, {[1] = {47}}},
  {"out10.STR", 1, {[1] = {47}}},
  {"out11.STR", 1, {[1] = {47}}},
  {"out12.STR", 1, {[1] = {47}}},
  {"7out13.STR", 1, {[1] = {47}}},
  {"out14.STR", 1, {[1] = {47}}},
  {"7out15.STR", 1, {[1] = {47}}},
  {"d01.STR", 1, {[1] = {47}}},
  {"d02.STR", 1, {[1] = {47}}},
  {"d03.STR", 1, {[1] = {47}}},
  {"d04.STR", 1, {[1] = {47}}},
  {"7d05.STR", 1, {[1] = {47}}},
  {"7d06.STR", 1, {[1] = {47}}},
  {"7d07.STR", 1, {[1] = {47}}},
  {"7d08.STR", 1, {[1] = {47}}},
  {"7d09.STR", 1, {[1] = {47}}},
  {"7d10.STR", 1, {[1] = {47}}},
  {"7d11.STR", 1, {[1] = {47}}},
  {"7d12.STR", 1, {[1] = {47}}},
  {"7d13.STR", 1, {[1] = {47}}},
  {"7d14.STR", 1, {[1] = {47}}},
  {"7d15.STR", 1, {[1] = {47}}},
  {"7d16.STR", 1, {[1] = {47}}},
  {"7d17.STR", 1, {[1] = {47}}},
  {"7d18.STR", 1, {[1] = {47}}},
  {"7d19.STR", 1, {[1] = {47}}},
  {"7d20.STR", 1, {[1] = {47}}},
  {"7d21.STR", 1, {[1] = {47}}},
  {"7d22.STR", 1, {[1] = {47}}},
  {"7d23.STR", 1, {[1] = {47}}},
  {"7d24.STR", 1, {[1] = {47}}},
  {"7d25.STR", 1, {[1] = {47}}},
  {"7d26.STR", 1, {[1] = {47}}},
  {"7d27.STR", 1, {[1] = {47}}},
  {"7d28.STR", 1, {[1] = {47}}},
  {"7d29.STR", 1, {[1] = {47}}},
  {"7d30.STR", 1, {[1] = {47}}},
  {"7d31.STR", 1, {[1] = {47}}},
  {"7d32.STR", 1, {[1] = {47}}},
  {"7d33.STR", 1, {[1] = {47}}},
  {"7d34.STR", 1, {[1] = {47}}},
  {"7d35.STR", 1, {[1] = {47}}},
  {"7d36.STR", 1, {[1] = {47}}},
  {"7d37.STR", 1, {[1] = {47}}},
  {"MDK01.STR", 1, {[1] = {47}}},
  {"MDK02.STR", 1, {[1] = {47}}},
  {"MDK03.STR", 1, {[1] = {47}}},
  {"MDK04.STR", 1, {[1] = {47}}},
  {"MDK05.STR", 1, {[1] = {47}}},
  {"MDT01.STR", 1, {[1] = {47}}},
  {"MDT02.STR", 1, {[1] = {47}}},
  {"MDT03.STR", 1, {[1] = {47}}},
  {"MDT04.STR", 1, {[1] = {47}}},
  {"MDT05.STR", 1, {[1] = {47}}},
  {"MDR01.STR", 1, {[1] = {47}}},
  {"MDR02.STR", 1, {[1] = {47}}},
  {"MDR03.STR", 1, {[1] = {47}}},
  {"MDR04.STR", 1, {[1] = {47}}},
  {"MDR05.STR", 1, {[1] = {47}}},
  {"MDT09.STR", 1, {[1] = {47}}},
  {"MDT10.STR", 1, {[1] = {47}}},
  {"MDT11.STR", 1, {[1] = {47}}},
  {"MDT12.STR", 1, {[1] = {47}}},
  {"MDT14.STR", 1, {[1] = {47}}},
  {"MDT15.STR", 1, {[1] = {47}}},
  {"t01.STR", 1, {[1] = {47}}},
  {"t02.STR", 1, {[1] = {47}}},
  {"t03.STR", 1, {[1] = {47}}},
  {"t04.STR", 1, {[1] = {47}}},
  {"OUTA1.STR", 1, {[1] = {47}}},
  {"OUTA2.STR", 1, {[1] = {47}}},
  {"OUTA3.STR", 1, {[1] = {47}}},
  {"OUTB1.STR", 1, {[1] = {47}}},
  {"OUTB2.STR", 1, {[1] = {47}}},
  {"OUTB3.STR", 1, {[1] = {47}}},
  {"OUTC1.STR", 1, {[1] = {47}}},
  {"OUTC2.STR", 1, {[1] = {47}}},
  {"OUTC3.STR", 1, {[1] = {47}}},
  {"OUTD1.STR", 1, {[1] = {47}}},
  {"OUTD2.STR", 1, {[1] = {47}}},
  {"OUTD3.STR", 1, {[1] = {47}}},
  {"OUTE1.STR", 1, {[1] = {47}}},
  {"OUTE2.STR", 1, {[1] = {47}}},
  {"OutE3.str", 1, {[1] = {47}}},
  {"6D01.STR", 1, {[1] = {47}}},
  {"6D02.str", 1, {[1] = {47}}},
  {"6D03.STR", 1, {[1] = {47}}},
  {"6D04.STR", 1, {[1] = {47}}},
  {"6D05.STR", 1, {[1] = {47}}},
  {"6D06.STR", 1, {[1] = {47}}},
  {"6D07.STR", 1, {[1] = {47}}},
  {"6D08.STR", 1, {[1] = {47}}},
  {"6D09.STR", 1, {[1] = {47}}},
  {"6D10.STR", 1, {[1] = {47}}},
  {"6D11.str", 1, {[1] = {47}}},
  {"6D12.STR", 1, {[1] = {47}}},
  {"6D13.STR", 1, {[1] = {47}}},
  {"6D14.STR", 1, {[1] = {47}}},
  {"6D15.STR", 1, {[1] = {47}}},
  {"6D16.STR", 1, {[1] = {47}}},
  {"6D17.STR", 1, {[1] = {47}}},
  {"6D18.STR", 1, {[1] = {47}}},
  {"6D19.STR", 1, {[1] = {47}}},
  {"6D20.STR", 1, {[1] = {47}}},
  {"6T1.STR", 1, {[1] = {47}}},
  {"6T2.STR", 1, {[1] = {47}}},
  {"6T3.STR", 1, {[1] = {47}}},
  {"6T4.STR", 1, {[1] = {47}}},
  {"6T5.STR", 1, {[1] = {47}}},
  {"6T6.STR", 1, {[1] = {47}}},
  {"6T7.STR", 1, {[1] = {47}}},
  {"6T8.STR", 1, {[1] = {47}}},
  {"CD1.STR", 1, {[1] = {47}}},
  {"CD2.STR", 1, {[1] = {47}}},
  {"CD3.STR", 1, {[1] = {47}}},
  {"SEWER.STR", 1, {[1] = {47}}},
  {"PYRAMID.STR", 1, {[1] = {47}}},
  {"ORACLE.STR", 1, {[1] = {47}}},
  {"SCI-FI.STR", 1, {[1] = {47}}},
  {"HIVE.STR", 1, {[1] = {47}}},
  {"ZARENA.STR", 1, {[1] = {47}}},
  {"ZDDB01.STR", 1, {[1] = {47}}},
  {"ZDDB02.STR", 1, {[1] = {47}}},
  {"ZDDB03.STR", 1, {[1] = {47}}},
  {"ZDDB04.STR", 1, {[1] = {47}}},
  {"ZDDB05.STR", 1, {[1] = {47}}},
  {"ZDDB06.STR", 1, {[1] = {47}}},
  {"ZDDB07.STR", 1, {[1] = {47}}},
  {"ZDDB08.STR", 1, {[1] = {47}}},
  {"ZDDB09.STR", 1, {[1] = {47}}},
  {"ZDDB10.STR", 1, {[1] = {47}}},
  {"ZDTL01.STR", 1, {[1] = {47}}},
  {"ZDTL02.STR", 1, {[1] = {47}}},
  {"ZDWJ01.STR", 1, {[1] = {47}}},
  {"ZDWJ02.STR", 1, {[1] = {47}}},
  {"ZDWJ1.STR", 1, {[1] = {47}}},
  {"ZDWJ2.STR", 1, {[1] = {47}}},
  {"ZNWC.STR", 1, {[1] = {47}}},
  {"Breach.STR", 1, {[1] = {47}}},
  {"BrAlvar.str", 1, {[1] = {47}}},
  {"BrBase.str", 1, {[1] = {47}}},
  {"7nwc.STR", 1, {[1] = {47}}},
}

local ctxt_exception_common = {
  [1] = {
    [8] = {
      --[312] = 50,
      [313] = 50,
      --[314] = 49,
      [319] = 50,
      [321] = 50,
      [324] = 50,
      [325] = 50,
      [326] = 50,
    },
  },
  --[[
  [13] = {
    [2] = {
      [5] = 52,
      [6] = 52,
      [7] = 52,
    },
  },
  ]]
}

local ctxt_exception = ctxt_exception_common

local function get_ctxt(txt, column, id, def)
  if txt == 13 then
    id = id + 1 - txts[txt][2]
    if column == 2 then
      local gender = Game.Bolster.Monsters[id] and Game.Bolster.Monsters[id].Gender
      if gender == 1 then
        return 52
      elseif gender == 2 then
        return 53
      else
        return 20
      end
    end
  end
  if ctxt_exception_common[txt] and ctxt_exception_common[txt][column]
      and ctxt_exception_common[txt][column][id] then
    return ctxt_exception_common[txt][column][id]
  end
  return def
end

local header1 = [[
# MMMerge aka MM678Merged, Revamp
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: MMMerge\n"
"Report-Msgid-Bugs-To: https://gitlab.com/cthscr/mmmerge/-/issues\n"
"POT-Creation-Date: ]]
local header2 = [[\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
]]

--"Language-Team: LANGUAGE <LL@li.org>\n"

msgs, msgctxts, msg_files = {}, {}, {}

local pot_creation_time

local function trail_dir_sep(dir)
  if strsub(dir, -1) ~= "/" then
    dir = dir .. "/"
  end
  return dir
end

local function line2msgid(line)
  local res, multi = string.gsub(string.gsub(line, "\"", "\\\""), "\n", "\\n\"\n\"")
  local end_ln
  if multi and multi > 0 then
    res, end_ln = string.gsub(res, "\n\"$", "")
    if end_ln and end_ln == 1 then
      if multi == 1 then
        res = "\"" .. res
      else
        res = "\"\"\n\"" .. res
      end
    else
      res = "\"\"\n\"" .. res .. "\""
    end
  else
    res = "\"" .. res .. "\""
  end
  return res
end

local function msgid2line(line, drop_q)
  local res = string.gsub(line, "^\"\"\n", "")
  res = string.sub(res, 2, strlen(res) - 1)
  res = string.gsub(res, "\\n\"\n\"", "\n")
  res = string.gsub(res, "\\n$", "\n")
  res = string.gsub(res, "\\\"", "\"")
  if drop_q then
    res = string.gsub(res, "^?[^:]+:", "")
  end
  return res
end

local function load_txt_src(src_txt_dir)
  src_txt_dir = src_txt_dir or src_txt_dir_default
  msgs, msgctxts, msg_files = {}, {}, {}
  for file_num, txt in ipairs(txts) do
    local ctxt_ex = ctxt_exception[file_num]
    local f = io.open(trail_dir_sep(src_txt_dir) .. txt[1], "rb")
    local data = f:read("*all")
    io.close(f)
    local lines = string.split(data, "\r\n")
    for line_num, line in ipairs(lines) do
      local words = string.split(line, "\t")
      if not words[1] or words[1] == "" then
        if txt[4] and txt[4] == 1 and line_num > txt[2] then
          break
        end
      elseif txt[2] and line_num >= txt[2] then
        for k, v in pairs(words) do
          if v ~= "" and txt[3] and txt[3][k] and (not txt[3][k][2] or line_num >= txt[3][k][2]) then
            --local ctxt = ctxt_ex and ctxt_ex[k] and ctxt_ex[k][line_num] or txt[3][k][1]
            local ctxt = get_ctxt(file_num, k, line_num, txt[3][k][1])
            local id
            if ctxt and msgctxts[ctxt] then
              for _, m in pairs(msgctxts[ctxt]) do
                if msgs[m][2] == v then
                  id = m
                  break
                end
              end
            end
            if id then
              table.insert(msgs[id][3], {file_num, line_num})
            else
              table.insert(msgs, {ctxt, v, {{file_num, line_num}}})
              msgctxts[ctxt] = msgctxts[ctxt] or {}
              table.insert(msgctxts[ctxt], #msgs)
              id = #msgs
            end
            msg_files[file_num] = msg_files[file_num] or {}
            msg_files[file_num][line_num] = msg_files[file_num][line_num] or {}
            msg_files[file_num][line_num][k] = id
          end
        end
      end
    end
  end
end

local function load_txt_loc(dir, src_txt_dir, no_load)
  src_txt_dir = src_txt_dir or src_txt_dir_default
  if not no_load then
    load_txt_src(src_txt_dir)
  end
  for file_num, txt in ipairs(txts) do
    local f = io.open(trail_dir_sep(dir) .. txt[1], "rb")
    if f then
      local data = f:read("*all")
      io.close(f)
      local lines = string.split(data, "\r\n")
      for line_num, line in ipairs(lines) do
        local words = string.split(line, "\t")
        if not words[1] or words[1] == "" then
          if txt[4] and txt[4] == 1 and line_num > txt[2] then
            break
          end
        elseif txt[2] and line_num >= txt[2] then
          for k, v in pairs(txt[3]) do
            if (not v[2] or line_num >= v[2]) and words[k] and words[k] ~= "" then
              local pos = msg_files[file_num] and msg_files[file_num][line_num] and msg_files[file_num][line_num][k]
              if pos then
                if not msgs[pos][5] then
                  msgs[pos][5] = words[k]
                elseif msgs[pos][5] ~= words[k] then
                  print("Warning: file \"" .. txt[1] .. "\", line " .. line_num
                    .. ", column " .. k .. " conflicts with other translation (\""
                    .. txts[msgs[pos][3][1][1]][1] .. "\":" .. msgs[pos][3][1][2] .. "):")
                  print("  '" .. words[k] .. "' vs. '" .. msgs[pos][5] .. "'")
                  print("  en: '" .. msgs[pos][2] .. "'")
                end
              else
                print("Error: no entry for file \"" .. txt[1] .. "\", line " .. line_num .. ", column " .. k)
                print("  '" .. words[k] .. "'")
              end
            end
          end
        end
      end
    else
      print("Error: no translated file " .. txt[1])
    end
  end
end

local function load_po(lang, po_dir, src_txt_dir, no_load)
  po_dir = po_dir or po_dir_default
  src_txt_dir = src_txt_dir or src_txt_dir_default
  if not no_load then
    load_txt_src(src_txt_dir)
  end
  local f = io.open(trail_dir_sep(po_dir) .. lang .. ".po", "rb")
  local data = f:read("*all")
  io.close(f)
  local lines = string.split(data, "\n")
  local stage = 0
  local msgflags, msgctxt, str, msgid, msgstr = {}, 0
  for line_num, line in ipairs(lines) do
    line = string.gsub(line, "\r", "")
    if line == "" then
      if stage == 2 then
        msgstr = msgid2line(str, true)
        if msgid == "" then
        elseif msgctxts[msgctxt] then
          local id
          for _, m in pairs(msgctxts[msgctxt]) do
            if msgs[m][2] == msgid then
              if table.find(msgflags, "fuzzy") then
                msgs[m][5] = ""
              else
                msgs[m][5] = msgstr
              end
              id = m
              break
            end
          end
          if not id then
            print("Warning: line " .. line_num .. ", no msgid '" .. msgid .. "'")
          end
        else
          print("Warning: no msgids for context " .. (msgctxt > 0 and context[msgctxt] or "0"))
        end
      else
      end
      stage = 0
      msgid, msgstr, msgflags, msgctxt = "", "", {}, 0
    else
      if string.sub(line, 1, 1) == "\"" then
        --print("stage " .. stage .. ", line " .. line)
        if stage == 1 or stage == 2 then
          str = str .. "\n" .. line
        else
          print("Warning: incorrect po file line " .. line_num)
        end
      elseif string.sub(line, 1, 1) == "#" then
        if string.sub(line, 1, 3) == "#, " then
          for _, v in pairs(string.split(string.sub(line, 4), ", ")) do
            if v ~= "" then
              table.insert(msgflags, v)
            end
          end
        end
      elseif string.sub(line, 1, 8) == "msgctxt " then
        msgctxt = table.find(context, string.sub(line, 10, -2)) or 0
      elseif string.sub(line, 1, 6) == "msgid " then
        stage = 1
        str = string.sub(line, 7)
      elseif string.sub(line, 1, 7) == "msgstr " then
        stage = 2
        msgid = msgid2line(str)
        str = string.sub(line, 8)
      end
    end
  end
end
MF.LoadPo = load_po

local function generate_pot(po_dir, src_txt_dir, no_load)
  po_dir = po_dir or po_dir_default
  src_txt_dir = src_txt_dir or src_txt_dir_default
  pot_creation_date = os.date("%Y-%m-%d %H:%M+0000", os.time())
  if not no_load then
    load_txt_src(src_txt_dir)
  end
  local pot = io.open(trail_dir_sep(po_dir) .. pot_file, "wb")
  pot:write(header1 .. pot_creation_date .. header2)
  for k, v in pairs(msgs) do
    pot:write("\n")
    local str, str2, last_str = ""
    for _, pos in pairs(v[3]) do
      str2 = txts[pos[1]][1] .. ":" .. pos[2]
      if str2 ~= last_str then
        if strlen(str) + strlen(str2) < 76 then
          if strlen(str) > 0 then
            str = str .. " " .. str2
          else
            str = str2
          end
        else
          pot:write("#: " .. str .. "\n")
          str = str2
        end
        last_str = str2
      end
    end
    pot:write("#: " .. str .. "\n")
    if v[1] and v[1] > 0 then
      pot:write("msgctxt \"" .. context[v[1]] .. "\"\n")
    end
    pot:write("msgid " .. line2msgid(v[2]) .. "\nmsgstr \"\"\n")
  end
  io.close(pot)
end
MF.GeneratePot = generate_pot

local function generate_po(lang, dir, po_dir, src_txt_dir, no_load)
  po_dir = po_dir or po_dir_default
  src_txt_dir = src_txt_dir or src_txt_dir_default
  if not no_load then
    load_txt_src(src_txt_dir)
  end
  pot_creation_date = pot_creation_date or os.date("%Y-%m-%d %H:%M+0000", os.time())
  load_txt_loc(dir, src_txt_dir, no_load)
  local po = io.open(trail_dir_sep(po_dir) .. lang .. ".po", "wb")
  po:write(header1 .. pot_creation_date .. header2)
  for k, v in pairs(msgs) do
    po:write("\n")
    local str, str2, last_str = ""
    for _, pos in pairs(v[3]) do
      str2 = txts[pos[1]][1] .. ":" .. pos[2]
      if str2 ~= last_str then
        if strlen(str) + strlen(str2) < 76 then
          if strlen(str) > 0 then
            str = str .. " " .. str2
          else
            str = str2
          end
        else
          po:write("#: " .. str .. "\n")
          str = str2
        end
        last_str = str2
      end
    end
    po:write("#: " .. str .. "\n")
    if v[1] and v[1] > 0 then
      po:write("msgctxt \"" .. context[v[1]] .. "\"\n")
    end
    po:write("msgid " .. line2msgid(v[2]) .. "\nmsgstr ")
    if v[5] and v[5] ~= "" then
      po:write(line2msgid(v[5]) .. "\n")
    else
      po:write("\"\"\n")
    end
  end
  io.close(po)
end
MF.GeneratePo = generate_po

local function generate_translated_txt(lang, dir, po_dir, src_txt_dir, no_load)
  po_dir = po_dir or po_dir_default
  src_txt_dir = src_txt_dir or src_txt_dir_default
  load_po(lang, po_dir, src_txt_dir, no_load)
  for file_num, txt in ipairs(txts) do
    local f = io.open(trail_dir_sep(src_txt_dir) .. txt[1], "rb")
    local data = f:read("*all")
    io.close(f)
    local lines = string.split(data, "\r\n")
    for line_num, line in ipairs(lines) do
      local words = string.split(line, "\t")
      if txt[2] and line_num >= txt[2] and words[1] and words[1] ~= "" then
        for k, v in pairs(txt[3]) do
          local pos = msg_files[file_num] and msg_files[file_num][line_num] and msg_files[file_num][line_num][k]
          if pos then
            if msgs[pos][5] ~= "" then
              words[k] = msgs[pos][5]
            end
          end
        end
        line = table.concat(words, "\t")
        lines[line_num] = line
      end
    end
    data = table.concat(lines, "\r\n")
    f = io.open(trail_dir_sep(dir) .. txt[1], "wb")
    f:write(data)
    io.close(f)
  end
end
MF.GenerateTranslatedTxt = generate_translated_txt

local function generate_txt_table(dir, filename, src_table, id1, id2, columns, start, src_txt_dir)
  src_txt_dir = src_txt_dir or src_txt_dir_default
  start = start or 1
  local f = io.open(src_table, "rb")
  local data = f:read("*all")
  io.close(f)
  local t = {}
  local lines = string.split(data, "\r\n")
  for line_num, line in ipairs(lines) do
    local words = string.split(line, "\t")
    local n = tonumber(words[id2])
    if n then
      t[n] = {}
      for k, v in pairs(columns) do
        t[n][k] = words[v]
      end
    end
  end
  f = io.open(trail_dir_sep(src_txt_dir) .. filename, "rb")
  local data = f:read("*all")
  io.close(f)
  lines = string.split(data, "\r\n")
  for line_num, line in ipairs(lines) do
    local words = string.split(line, "\t")
    if line_num >= start and words[1] and words[1] ~= "" then
      local n = tonumber(words[id1])
      if n and t[n] then
        for k, v in pairs(columns) do
          if t[n][k] then
            words[k] = t[n][k]
          end
        end
        line = table.concat(words, "\t")
        lines[line_num] = line
      end
    end
  end
  data = table.concat(lines, "\r\n")
  f = io.open(trail_dir_sep(dir) .. filename, "wb")
  f:write(data)
  io.close(f)
end
MF.GenerateTxtTable = generate_txt_table

