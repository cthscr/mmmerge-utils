
local i4, u1, u2, u4 = mem.i4, mem.u1, mem.u2, mem.u4
local strformat = string.format

function events.EvtGlobal(evt_id)
	Log(Merge.Log.Info, "[%d] Global event %d", Game.Time, evt_id)
end

function events.EvtMap(evt_id)
	Log(Merge.Log.Info, "[%d] Map %d (%s) event %d", Game.Time, Map.MapStatsIndex, Map.Name, evt_id)
end

--[[
function events.GetEventHint(evt_id, hint, house_id)
	Log(Merge.Log.Info, "GetEventHint for event %d: %s, %s", evt_id,
		hint or "nil",
		house_id and tostring(house_id) or "nil")
end
]]

function events.Action(t)
	Log(Merge.Log.Info, "[%d] Action %d, Param %d, Param2 %d; %d", Game.Time, t.Action, t.Param, t.Param2, Game.CtrlPressed and 1 or 0)
end

function events.MenuAction(t)
	Log(Merge.Log.Info, "MenuAction %d, Param %d, Param2 %d; %d", t.Action, t.Param, t.Param2, Game.CtrlPressed and 1 or 0)
end

local function evt_log_hooks()
	mem.autohook2(0x443895, function(d)
		local cmd_id = d.ecx + 1
		--local line = u1[d.esi + 3]
		local param1, param2, param3
		local logstr = ""
		-- Implying little-endian for params
		if cmd_id == 14 then  -- Cmp
			param1 = u2[d.esi + 5]
			param2 = u4[d.esi + 7]
			-- param3 = ui[d.esi + 11]
			logstr = strformat(", %d, %d", param1, param2)
		elseif cmd_id == 16 or cmd_id == 17 or cmd_id == 18 then  -- Add, Subtract, Set
			param1 = u2[d.esi + 5]
			param2 = u4[d.esi + 7]
			logstr = strformat(", %d, %d", param1, param2)
		elseif cmd_id == 22 then  -- SpeakNPC
			param1 = i4[d.esi + 5]
			logstr = strformat(", %d", param1)
		elseif cmd_id == 39 then  -- SetNPCTopic
			param1 = i4[d.esi + 5]
			param2 = u1[d.esi + 9]
			param3 = i4[d.esi + 10]
			logstr = strformat(", %d, %d", param1, param2)
		elseif cmd_id == 40 then  -- MoveNPC
			param1 = i4[d.esi + 5]
			param2 = i4[d.esi + 9]
			logstr = strformat(", %d, %d", param1, param2)
		end
		Log(Merge.Log.Info, "evt command %d%s", cmd_id, logstr)
	end)
end

function events.GameInitialized1()
	evt_log_hooks()
end
