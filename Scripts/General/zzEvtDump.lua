local MF = Merge.Functions
--MF = {}
local floor = math.floor
local strbyte, strchar, strformat, strlen = string.byte, string.char, string.format, string.len
local strsplit, strsub = string.split, string.sub

local cmd_names = {
	--[0] = "Cmd00",
	[1] = "Exit",
	[2] = "EnterHouse",
	[3] = "PlaySound",
	[4] = "Hint",
	[5] = "MazeInfo",
	[6] = "MoveToMap",
	[7] = "OpenChest",
	[8] = "FaceExpression",
	[9] = "DamagePlayer",
	[10] = "SetSnow",
	[11] = "SetTexture",
	[12] = "ShowMovie",
	[13] = "SetSprite",
	[14] = "Cmp",
	[15] = "SetDoorState",
	[16] = "Add",
	[17] = "Subtract",
	[18] = "Set",
	[19] = "SummonMonsters",
	--[20] = "Cmd14"
	[21] = "CastSpell",
	[22] = "SpeakNPC",
	[23] = "SetFacetBit",
	[24] = "SetMonsterBit",
	[25] = "RandomGoTo",
	[26] = "Question",
	--[27] = "Cmd1B",
	--[28] = "Cmd1C",
	[29] = "StatusText",
	[30] = "SetMessage",
	[31] = "OnTimer",
	[32] = "SetLight",
	[33] = "SimpleMessage",
	[34] = "SummonObject",
	[35] = "ForPlayer",
	[36] = "GoTo",
	[37] = "OnLoadMap",
	[38] = "OnLongTimer",
	[39] = "SetNPCTopic",
	[40] = "MoveNPC",
	[41] = "GiveItem",
	[42] = "ChangeEvent",
	[43] = "CheckSkill",
	[44] = "CanShowTopic.Cmp",
	[45] = "CanShowTopic.Exit",
	[46] = "CanShowTopic.Set",
	[47] = "SetNPCGroupNews",
	[48] = "SetMonsterGroup",
	[49] = "SetNPCItem",
	[50] = "SetNPCGreeting",
	[51] = "CheckMostersKilled",
	[52] = "CanShowTopic.CheckMonstersKilled",
	[53] = "OnLeaveMap",
	[54] = "ChangeGroupToGroup",
	[55] = "ChangeGroupAlly",
	[56] = "CheckSeason",
	[57] = "SetMonGroupBit",
	[58] = "SetChestBit",
	[59] = "FaceAnimation",
	[60] = "SetMonsterItem",
	[61] = "OnDateTimer",
	[62] = "EnableDateTimer",
	[63] = "StopDoor",
	[64] = "CheckItemsCount",
	[65] = "RemoveItems",
	[66] = "Jump",
	[67] = "IsTotalBountyInRange",
	[68] = "CanPlayerAct"
}

local cmd_vars_merge = {
	[1] = "SexIs",
	[2] = "ClassIs",
	[3] = "HP",
	[4] = "HasFullHP",
	[5] = "SP",
	[6] = "HasFullSP",
	[7] = "ArmorClass",
	[8] = "ArmorClassBonus",
	[9] = "BaseLevel",
	[10] = "LevelBonus",
	[11] = "AgeBonus",
	[12] = "Awards",
	[13] = "Experience",
	[16] = "QBits",
	[17] = "Inventory",
	[18] = "HourIs",
	[19] = "DayOfYearIs",
	[20] = "DayOfWeekIs",
	[21] = "Gold",
	[22] = "GoldAddRandom",
	[23] = "Food",
	[24] = "FoodAddRandom",
	[25] = "MightBonus",
	[26] = "IntellectBonus",
	[27] = "PeronalityBonus",
	[28] = "EnduranceBonus",
	[29] = "SpeedBonus",
	[30] = "AccuracyBonus",
	[31] = "LuckBonus",
	[32] = "BaseMight",
	[33] = "BaseIntellect",
	[34] = "BasePersonality",
	[35] = "BaseEndurance",
	[36] = "BaseSpeed",
	[37] = "BaseAccuracy",
	[38] = "BaseLuck",
	[39] = "CurrentMight",
	[40] = "CurrentIntellect",
	[41] = "CurrentPersonality",
	[42] = "CurrentEndurance",
	[43] = "CurrentSpeed",
	[44] = "CurrentAccuracy",
	[45] = "CurrentLuck",
	[46] = "FireResistance",
	[47] = "AirResistance",
	[48] = "WaterResistance",
	[49] = "EarthResistance",
	[50] = "SpiritResistance",
	[51] = "MindResistance",
	[52] = "BodyResistance",
	[53] = "LightResistance",
	[54] = "DarkResistance",
	--[55] = "PhysResistance",
	[56] = "MagicResistance",	-- EnerResistance
	[57] = "FireResBonus",
	[58] = "AirResBonus",
	[59] = "WaterResBonus",
	[60] = "EarthResBonus",
	[61] = "SpiritResBonus",
	[62] = "MindResBonus",
	[63] = "BodyResBonus",
	[64] = "LightResBonus",
	[65] = "DarkResBonus",
	--[66] = "PhysResBonus",
	[67] = "MagicResBonus",	-- EnerResBonus
	[68] = "StaffSkill",
	[69] = "SwordSkill",
	[70] = "DaggerSkill",
	[71] = "AxeSkill",
	[72] = "SpearSkill",
	[73] = "BowSkill",
	[74] = "MaceSkill",
	[75] = "BlasterSkill",
	[76] = "ShieldSkill",
	[77] = "LeatherSkill",
	[78] = "ChainSkill",
	[79] = "PlateSkill",
	[80] = "FireSkill",
	[81] = "AirSkill",
	[82] = "WaterSkill",
	[83] = "EarthSkill",
	[84] = "SpiritSkill",
	[85] = "MindSkill",
	[86] = "BodySkill",
	[87] = "LightSkill",
	[88] = "DarkSkill",
	[89] = "DarkElfAbilitySkill",
	[90] = "VampireAbilitySkill",
	[91] = "DragonAbilitySkill",
	[92] = "IdentifyItemSkill",
	[93] = "MerchantSkill",
	[94] = "RepairSkill",
	[95] = "BodybuildingSkill",
	[96] = "MeditationSkill",
	[97] = "PerceptionSkill",
	[98] = "RegenerationSkill",
	[99] = "DisarmTrapsSkill",
	[100] = "DodgingSkill",
	[101] = "UnarmedSkill",
	[102] = "IdentifyMonsterSkill",
	[103] = "ArmsmasterSkill",
	[104] = "StealingSkill",
	[105] = "AlchemySkill",
	[106] = "LearningSkill",
	[107] = "Cursed",
	[108] = "Weak",
	[109] = "Asleep",
	[110] = "Afraid",
	[111] = "Drunk",
	[112] = "Insane",
	[113] = "PoisonedGreen",
	[114] = "DiseasedGreen",
	[115] = "PoisonedYellow",
	[116] = "DiseasedYellow",
	[117] = "PoisonedRed",
	[118] = "DiseasedRed",
	[119] = "Paralysed",
	[120] = "Unconscious",
	[121] = "Dead",
	[122] = "Stoned",
	[123] = "Eradicated",
	[124] = "MainCondition",
	[125] = "MapVar0",
	[126] = "MapVar1",
	[127] = "MapVar2",
	[128] = "MapVar3",
	[129] = "MapVar4",
	[130] = "MapVar5",
	[131] = "MapVar6",
	[132] = "MapVar7",
	[133] = "MapVar8",
	[134] = "MapVar9",
	[135] = "MapVar10",
	[136] = "MapVar11",
	[137] = "MapVar12",
	[138] = "MapVar13",
	[139] = "MapVar14",
	[140] = "MapVar15",
	[141] = "MapVar16",
	[142] = "MapVar17",
	[143] = "MapVar18",
	[144] = "MapVar19",
	[145] = "MapVar20",
	[146] = "MapVar21",
	[147] = "MapVar22",
	[148] = "MapVar23",
	[149] = "MapVar24",
	[150] = "MapVar25",
	[151] = "MapVar26",
	[152] = "MapVar27",
	[153] = "MapVar28",
	[154] = "MapVar29",
	[155] = "MapVar30",
	[156] = "MapVar31",
	[157] = "MapVar32",
	[158] = "MapVar33",
	[159] = "MapVar34",
	[160] = "MapVar35",
	[161] = "MapVar36",
	[162] = "MapVar37",
	[163] = "MapVar38",
	[164] = "MapVar39",
	[165] = "MapVar40",
	[166] = "MapVar41",
	[167] = "MapVar42",
	[168] = "MapVar43",
	[169] = "MapVar44",
	[170] = "MapVar45",
	[171] = "MapVar46",
	[172] = "MapVar47",
	[173] = "MapVar48",
	[174] = "MapVar49",
	[175] = "MapVar50",
	[176] = "MapVar51",
	[177] = "MapVar52",
	[178] = "MapVar53",
	[179] = "MapVar54",
	[180] = "MapVar55",
	[181] = "MapVar56",
	[182] = "MapVar57",
	[183] = "MapVar58",
	[184] = "MapVar59",
	[185] = "MapVar60",
	[186] = "MapVar61",
	[187] = "MapVar62",
	[188] = "MapVar63",
	[189] = "MapVar64",
	[190] = "MapVar65",
	[191] = "MapVar66",
	[192] = "MapVar67",
	[193] = "MapVar68",
	[194] = "MapVar69",
	[195] = "MapVar70",
	[196] = "MapVar71",
	[197] = "MapVar72",
	[198] = "MapVar73",
	[199] = "MapVar74",
	[200] = "MapVar75",
	[201] = "MapVar76",
	[202] = "MapVar77",
	[203] = "MapVar78",
	[204] = "MapVar79",
	[205] = "MapVar80",
	[206] = "MapVar81",
	[207] = "MapVar82",
	[208] = "MapVar83",
	[209] = "MapVar84",
	[210] = "MapVar85",
	[211] = "MapVar86",
	[212] = "MapVar87",
	[213] = "MapVar88",
	[214] = "MapVar89",
	[215] = "MapVar90",
	[216] = "MapVar91",
	[217] = "MapVar92",
	[218] = "MapVar93",
	[219] = "MapVar94",
	[220] = "MapVar95",
	[221] = "MapVar96",
	[222] = "MapVar97",
	[223] = "MapVar98",
	[224] = "MapVar99",
	[225] = "AutonoteBits",
	[233] = "PlayerBits",
	[234] = "NPCs",
	[235] = "ReputationIs",
	[236] = "DaysCounter1",
	[237] = "DaysCounter2",
	[238] = "DaysCounter3",
	[239] = "DaysCounter4",
	[240] = "DaysCounter5",
	[241] = "DaysCounter6",
	[242] = "Flying",
	[243] = "HasNPCProfession",
	[244] = "TotalCircusPrize",
	[245] = "SkillPoints",
	[246] = "MonthIs",
	[247] = "Counter1",
	[248] = "Counter2",
	[249] = "Counter3",
	[250] = "Counter4",
	[251] = "Counter5",
	[252] = "Counter6",
	[253] = "Counter7",
	[254] = "Counter8",
	[255] = "Counter9",
	[256] = "Counter10",
	[257] = "SpecialDate1",
	[258] = "SpecialDate2",
	[259] = "SpecialDate3",
	[260] = "SpecialDate4",
	[261] = "SpecialDate5",
	[262] = "SpecialDate6",
	[263] = "SpecialDate7",
	[264] = "SpecialDate8",
	[265] = "SpecialDate9",
	[266] = "SpecialDate10",
	[267] = "SpecialDate11",
	[268] = "SpecialDate12",
	[269] = "SpecialDate13",
	[270] = "SpecialDate14",
	[271] = "SpecialDate15",
	[272] = "SpecialDate16",
	[273] = "SpecialDate17",
	[274] = "SpecialDate18",
	[275] = "SpecialDate19",
	[276] = "SpecialDate20",
	[277] = "Reputation",
	[278] = "History1",
	[279] = "History2",
	[280] = "History3",
	[281] = "History4",
	[282] = "History5",
	[283] = "History6",
	[284] = "History7",
	[285] = "History8",
	[286] = "History9",
	[287] = "History10",
	[288] = "History11",
	[289] = "History12",
	[290] = "History13",
	[291] = "History14",
	[292] = "History15",
	[293] = "History16",
	[294] = "History17",
	[295] = "History18",
	[296] = "History19",
	[297] = "History20",
	[298] = "History21",
	[299] = "History22",
	[300] = "History23",
	[301] = "History24",
	[302] = "History25",
	[303] = "History26",
	[304] = "History27",
	[305] = "History28",
	[306] = "History29",
	[307] = "MapAlert",
	[308] = "BankGold",
	[309] = "Deaths",
	[310] = "MonsterHunted",
	[311] = "PrisonTerms",
	[312] = "ArenaWinsPage",
	[313] = "ArenaWinsSquire",
	[314] = "ArenaWinsKnight",
	[315] = "ArenaWinsLord",
	[316] = "Invisible",
	[317] = "IsWearingItem",
	[318] = "Players"
}

local cmd_size = {
	[0] = 0,
	[1] = 1,
	[2] = 4,
	[3] = 12,
	[4] = 1,
	[5] = 1,
	[6] = 28,
	[7] = 1,
	[8] = 2,
	[9] = 6,
	[10] = 2,
	[11] = 4,
	[12] = 2,
	[13] = 6,
	[14] = 7,
	[15] = 2,
	[16] = 6,
	[17] = 6,
	[18] = 6,
	[19] = 23,
	[20] = 5,
	[21] = 27,
	[22] = 4,
	[23] = 9,
	[24] = 9,
	[25] = 6,
	[26] = 13,
	[27] = 2,
	[28] = 1,
	[29] = 4,
	[30] = 4,
	[31] = 10,
	[32] = 5,
	[33] = 1,
	[34] = 22,
	[35] = 1,
	[36] = 1,
	[37] = 1,
	[38] = 10,
	[39] = 9,
	[40] = 8,
	[41] = 6,
	[42] = 4,
	[43] = 7,
	[44] = 7,
	[45] = 1,
	[46] = 1,
	[47] = 8,
	[48] = 8,
	[49] = 9,
	[50] = 8,
	[51] = 8,
	[52] = 8,
	[53] = 1,
	[54] = 8,
	[55] = 8,
	[56] = 2,
	[57] = 9,
	[58] = 9,
	[59] = 2,
	[60] = 9,
	[61] = 8,
	[62] = 3,
	[63] = 4,
	[64] = 7,
	[65] = 6,
	[66] = 8,
	[67] = 9,
	[68] = 5
}

local cmd_str = {
	[6] = 1,
	[11] = 1,
	[12] = 1,
	[13] = 1
}
--[[
local cmd_var = {
	[14] = 1,
	[16] = 1,
	[17] = 1,
	[18] = 1,
	[44] = 1
}
]]
local arg_len = {
  [0] = 1, -- unknown 1 byte
  [1] = 1, -- u1
  [2] = 1, -- i1
  [3] = 2, -- u2
  [4] = 2, -- i2
  [5] = 4, -- u4
  [6] = 4, -- i4
  [7] = 0, -- char[]
}

local arg_type = {
  [0] = 0, -- (u1) unknown
  [1] = 1, -- (u1) misc
  [2] = 2, -- (i1) misc
  [3] = 3, -- (u2) misc
  [4] = 4, -- (i2) misc
  [5] = 5, -- (u4) misc
  [6] = 6, -- (i4) misc

  [11] = 1, -- (u1) misc print
  [12] = 2, -- (i1) misc print
  [13] = 3, -- (u2) misc print
  [14] = 4, -- (i2) misc print
  [15] = 5, -- (u4) misc print
  [16] = 6, -- (i4) misc print

  [21] = 1, -- (u1) line
  [22] = 1, -- (u1) player (party slot)
  [23] = 3, -- (u2) evt var
  [24] = 6, -- (i4) evt var value
  [25] = 5, -- (u4) house_id
  [26] = 3, -- (u2) house_id (MoveToMap mm8)
  [27] = 1, -- (u1) house_id (MoveToMap mm6,mm7)
  [28] = 6, -- (i4) sound_id
  [29] = 1, -- (u1) face expression
  [30] = 1, -- (u1) damage type
  [31] = 6, -- (i4) map_x
  [32] = 6, -- (i4) map_y
  [33] = 6, -- (i4) map_z
  [34] = 6, -- (i4) direction
  [35] = 6, -- (i4) look_angle
  [36] = 6, -- (i4) speed
  [37] = 6, -- (i4) facet group
  [38] = 6, -- (i4) facet index (mm6)
  [39] = 6, -- (i4) model index (mm6)
  [40] = 6, -- (i4) sprite_id
  [41] = 6, -- (i4) npc group
  [42] = 1, -- (u1) spell_id
  [43] = 1, -- (u1) skill mastery
  [44] = 1, -- (u1) skill level
  [45] = 6, -- (i4) npc_id
  [46] = 6, -- (i4) map monster index
  [47] = 5, -- (u4) map monster index
  [48] = 6, -- (i4) string_id
  [49] = 5, -- (u4) bit
  [50] = 6, -- (i4) bit
  [51] = 6, -- (i4) light group
  [52] = 6, -- (i4) light index
  [53] = 6, -- (i4) item ref (id or kind+strength)
  [54] = 6, -- (i4) object kind index
  [55] = 6, -- (i4) npc_topic_id / event_id
  [56] = 1, -- (u1) item type
  [57] = 5, -- (u4) item_id
  [58] = 1, -- (u1) item type or enchantment type
  [59] = 1, -- (u1) skill_id
  [60] = 6, -- (i4) skill level
  [61] = 5, -- (u4) npc group
  [62] = 5, -- (u4) npc_news_id
  [63] = 6, -- (i4) item_id
  [64] = 6, -- (i4) npc_greet_id
  [65] = 1, -- (u1) monster killed check type
  [66] = 5, -- (u4) monster killed check id
  [67] = 6, -- (i4) ally (monster kind/class; neg is npc group in revamp)
  [68] = 1, -- (u1) face animation
  [69] = 1, -- (u1) date timer id
  [70] = 3, -- (u2) date timer mod id
  [71] = 3, -- (u2) item_id
  [72] = 4, -- (i2) direction
  [73] = 4, -- (i2) z angle
  [74] = 6, -- (i4) gold
  [75] = 5, -- (u4) roster_id

  [129] = 7, -- (char[]) map_name
  [130] = 7, -- (char[]) texture_name
  [131] = 7, -- (char[]) movie_name
  [132] = 7, -- (char[]) sprite_name
}

local cmd_args = {
  [1] = {0}, -- Exit
  [2] = {25}, -- EnterHouse
  [3] = {28, 31, 32}, -- PlaySound
  [4] = {11}, -- Hint
  [5] = {11}, -- MazeInfo
  [6] = {31, 32, 33, 6, 6, 6, 26, 1, 129}, -- MoveToMap
  [7] = {11}, -- OpenChest
  [8] = {22, 29}, -- FaceExpression
  [9] = {22, 30, 16}, -- DamagePlayer
  [10] = {11, 11}, -- SetSnow
  [11] = {37, 130}, -- SetTexture
  [12] = {11, 11, 131}, -- ShowMovie
  [13] = {40, 11, 132}, -- SetSprite
  [14] = {23, 24, 21}, -- Cmp
  [15] = {11, 11}, -- SetDoorState
  [16] = {23, 24}, -- Add
  [17] = {23, 24}, -- Subtract
  [18] = {23, 24}, -- Set
  [19] = {11, 11, 11, 31, 32, 33, 41, 16}, -- SummonMonsters
  [20] = {6, 1}, -- Cmd14
  [21] = {42, 43, 44, 31, 32, 33, 31, 32, 33}, -- CastSpell
  [22] = {45}, -- SpeakNPC
  [23] = {37, 49, 11}, -- SetFacetBit
  [24] = {46, 50, 11}, -- SetMonsterBit
  [25] = {21, 21, 21, 21, 21, 21}, -- RandomGoTo
  [26] = {48, 48, 48, 21}, -- Question
  [27] = {1, 1}, -- Cmd1B
  [28] = {1}, -- Cmd1C
  [29] = {48}, -- StatusText
  [30] = {48}, -- SetMessage
  [31] = {11, 11, 11, 11, 11, 11, 13, 1, 1}, -- OnTimer
  [32] = {51, 11}, -- SetLight
  [33] = {1}, -- SimpleMessage
  [34] = {53, 31, 32, 33, 16, 11, 11}, -- SummonObject
  [35] = {22}, -- ForPlayer
  [36] = {21}, -- GoTo
  [37] = {1}, -- OnLoadMap
  [38] = {11, 11, 11, 11, 11, 11, 13, 1, 1}, -- OnRefillTimer
  [39] = {45, 11, 55}, -- SetNPCTopic
  [40] = {45, 25}, -- MoveNPC
  [41] = {11, 58, 57}, -- GiveItem
  [42] = {55}, -- ChangeEvent
  [43] = {59, 43, 60, 21}, -- CheckSkill
  -- MM7/8
  [44] = {23, 24, 21}, -- CanShowTopic.Cmp
  [45] = {1}, -- CanShowTopic.Exit
  [46] = {11}, -- CabShowTopic.Set
    [47] = {61, 62}, -- SetNPCGroupNews
  [48] = {57, 61}, -- SetMonsterGroup
  [49] = {45, 63, 11}, -- SetNPCItem
  [50] = {45, 64}, -- SetNPCGreeting
  [51] = {65, 66, 11, 11, 21}, -- CheckMonstersKilled
  [52] = {65, 66, 11, 11, 21}, -- CanShowTopic.CheckMonsterKilled
  [53] = {1}, -- OnLeaveMap
  [54] = {61, 61}, -- ChangeGroupToGroup
  [55] = {61, 67}, -- ChangeGroupAlly
  [56] = {11, 21}, -- CheckSeason
  [57] = {61, 49, 11}, -- SetMonGroupBit
  [58] = {16, 49, 11}, -- SetChestBit
  [59] = {22, 68}, -- FaceAnimation
  [60] = {46, 63, 11}, -- SetMonsterItem
  -- MM8
  [61] = {69, 11, 11, 11, 11, 13, 11}, -- OnDateTimer
  [62] = {70, 11}, -- EnableDateTimer
  [63] = {16}, -- StopDoor
  [64] = {71, 71, 13, 21}, -- CheckItemsCount
  [65] = {71, 71, 13}, -- RemoveItems
  [66] = {72, 73, 36}, -- Jump
  [67] = {74, 74, 21}, -- IsTotalBountyInRange
  [68] = {75, 21}, -- CanPlayerAct
}

local cmd_args7 = {
  [6] = {31, 32, 33, 6, 6, 6, 27, 1, 0, 129}, -- MoveToMap
  [32] = {52, 11}, -- SetLight
  [34] = {54, 31, 32, 33, 16, 11, 11}, -- SummonObject
  [51] = {65, 66, 11, 21}, -- CheckMonstersKilled
  [52] = {65, 66, 11, 21}, -- CanShowTopic.CheckMonstersKilled
}

local cmd_args6 = {
  [6] = {31, 32, 33, 6, 6, 6, 27, 1, 0, 129}, -- MoveToMap
  [11] = {38, 130}, -- SetTexture (indoor)
    [12] = {39, 38, 130}, -- SetTextureOutdoors
  [19] = {11, 11, 11, 31, 32, 33}, -- SummonMonsters
  [23] = {38, 49, 11}, -- SetFacetBit (indoor)
    [24] = {39, 38, 50, 11}, -- SetFacetBitOutdoor
  [32] = {52, 11}, -- SetLight
  [34] = {54, 31, 32, 33, 16, 11, 11}, -- SummonObject
}

local function read_u1(data, pos)
  return strbyte(data, pos)
end

local function read_u2(data, pos)
  return strbyte(data, pos) + strbyte(data, pos + 1) * 0x100
end

local function read_u4(data, pos)
  return strbyte(data, pos) + strbyte(data, pos + 1) * 0x100
    + strbyte(data, pos + 2) * 0x10000 + strbyte(data, pos + 3) * 0x1000000
end

local function read_i1(data, pos)
  local res = read_u1(data, pos)
  if res > 0x7F then
	res = res - 0x100
  end
  return res
end

local function read_i2(data, pos)
  local res = read_u2(data, pos)
  if res > 0x7FFF then
	res = res - 0x10000
  end
  return res
end

local function read_i4(data, pos)
  local res = read_u4(data, pos)
  if res > 0x7FFFFFFF then
	res = res - 0x100000000
  end
  return res
end

local function print_u1(val)
  return strformat(" %02X", val)
end

local function print_u2(val)
  return strformat(" %02X %02X", bit.band(val, 0xFF),
    bit.band(bit.rshift(val, 8), 0xFF))
end

local function print_u4(val)
  return strformat(" %02X %02X %02X %02X", bit.band(val, 0xFF),
    bit.band(bit.rshift(val, 8), 0xFF),
    bit.band(bit.rshift(val, 16), 0xFF),
    bit.band(bit.rshift(val, 24), 0xFF))
end

local function print_i1(val)
  if val < 0 then
    val = val + 0x100
  end
  return print_u1(val)
end

local function print_i2(val)
  if val < 0 then
    val = val + 0x10000
  end
  return print_u2(val)
end

local function print_i4(val)
  if val < 0 then
    val = val + 0x100000000
  end
  return print_u4(val)
end

local function print_str(val)
  str = ""
  for i = 1, strlen(val) do
    str = str .. strformat(" %02X", strbyte(val, i))
  end
  return str .. " 00"
end

local function read_arg(arg_t, data, pos)
  if arg_type[arg_t] == 0 or arg_type[arg_t] == 1 then
    return read_u1(data, pos)
  elseif arg_type[arg_t] == 2 then
    return read_i1(data, pos)
  elseif arg_type[arg_t] == 3 then
    return read_u2(data, pos)
  elseif arg_type[arg_t] == 4 then
    return read_i2(data, pos)
  elseif arg_type[arg_t] == 5 then
    return read_u4(data, pos)
  elseif arg_type[arg_t] == 6 then
    return read_i4(data, pos)
  elseif arg_type[arg_t] == 7 then
    return strsub(data, pos, -2)
  end
end

local function print_arg(arg_t, val)
  if arg_type[arg_t] == 0 or arg_type[arg_t] == 1 then
    return print_u1(val)
  elseif arg_type[arg_t] == 2 then
    return print_i1(val)
  elseif arg_type[arg_t] == 3 then
    return print_u2(val)
  elseif arg_type[arg_t] == 4 then
    return print_i2(val)
  elseif arg_type[arg_t] == 5 then
    return print_u4(val)
  elseif arg_type[arg_t] == 6 then
    return print_i4(val)
  elseif arg_type[arg_t] == 7 then
    return print_str(val)
  end
end

local function print_arg_note(arg_t, val)
  if arg_t == 23 then
    return cmd_vars_merge[val] or val
  elseif arg_t >= 11 and arg_t <= 16 then
    return val
  elseif table.find({22}, arg_t) then
    return val
  elseif arg_t == 49 or arg_t == 50 then
    return strformat("0x%x", val)
  elseif agr_t == 53 then
    return val
  elseif arg_t >= 24 and arg_t <= 75 then
    return val
  elseif arg_t >= 129 and arg_t <= 132 then
    return val
  end
end

local function evt2dump(evt, txt, print_line)
	local line, data, cmd, var, bt, len = 0
	local cmd_len, cmd_line
	local ef = io.open(evt, "rb")
	local tf = io.open(txt, "w")
	while true do
		line = line + 1
		cmd_len = ef:read(1)
		if cmd_len == nil then
			break
		else
			cmd_len = strbyte(cmd_len)
		end
		if cmd_len < 4 then
			print("Length " .. cmd_len .. " at line " .. line)
			break
		end
		data = ef:read(4)
		if not data then
			print("Missing header byte(s) at line " .. line)
			break
		end
		tf:write("#" .. (print_line and (line .. " ") or "") .. (strbyte(data, 1) + strbyte(data, 2) * 256) .. ":" .. strbyte(data, 3) .. "\n")
		cmd = strbyte(data, 4)
		if cmd_size[cmd] then
			if cmd_len ~= cmd_size[cmd] + 4 and not cmd_str[cmd] then
				print("Wrong cmd len at line " .. line)
				break
			end
		else
			print("Unknown cmd byte at line " .. line)
			break
		end
		cmd_line = ef:read(cmd_len - 4)
		if not cmd_line then
			print("Cannot read operand bytes at line " .. line)
			break
		end
		if cmd_names[cmd] then
			tf:write("#  " .. cmd_names[cmd])
		end

		if cmd_args[cmd] then
			local pos, argv, str, res = 1, {}, ""
			for k, v in ipairs(cmd_args[cmd]) do
				argv[k] = read_arg(v, cmd_line, pos)
				pos = pos + arg_len[arg_type[v]]
				res = print_arg_note(v, argv[k])
				if res then
					str = str .. " " .. res
				end
			end
			tf:write(str .. "\n")
			tf:write(strformat("%02X", cmd_len))
			for i = 1, 4 do
				tf:write(strformat(" %02X", strbyte(data, i)))
			end
			str = ""
			for k, v in ipairs(cmd_args[cmd]) do
				str = str .. print_arg(v, argv[k])
			end
			tf:write(str .. "\n")
		else
			if cmd_names[cmd] then
				tf:write("\n")
				print("No scheme for command " .. cmd)
			else
				print("Unknown command " .. cmd)
			end
			tf:write(strformat("%02X", cmd_len))
			for i = 1, 4 do
				tf:write(strformat(" %02X", strbyte(data, i)))
			end
			for i = 1, cmd_len - 4 do
				tf:write(strformat(" %02X", strbyte(cmd_line, i)))
			end
			--[[
			if cmd_size[cmd] then
				for i = 1, cmd_size[cmd] do
					bt = strbyte(cmd_line, i)
					if bt then
						tf:write(strformat(" %02X", bt))
					else
						print("Missing byte(s) at line " .. line .. "(cmd " .. cmd .. ")")
						break
					end
				end
			end
			if cmd_str[cmd] then
				len = cmd_size[cmd]
				while true do
					len = len + 1
					bt = strbyte(cmd_line, len)
					if bt then
						tf:write(strformat(" %02X", bt))
						if bt == 0 then
							break
						end
					else
						print("Missing byte(s) at line " .. line .. "(cmd " .. cmd .. ")")
						break
					end
				end
			end
			]]
			tf:write("\n")
		end
	end
	io.close(ef)
	io.close(tf)
end
MF.evt2dump = evt2dump

local function dump2evt(txt, evt)
	local tf = io.open(txt, "r")
	local ef = io.open(evt, "wb")
	local iter = tf:lines()
	for line in iter do
		if line:byte(1) ~= 35 then	-- "#"
			local bytes = strsplit(line, " ")
			for i = 1, #bytes do
				ef:write(strchar(tonumber("0x" .. bytes[i])))
			end
		end
	end
	io.close(ef)
	io.close(tf)
end
MF.dump2evt = dump2evt

local function evt2dump_dir(edir, tdir, print_line)
	for f in path.find(edir .. "/*.evt") do
		local name = path.name(f)
		evt2dump(edir .. "/" .. name, tdir .. "/" .. path.setext(name, ".txt"), print_line)
	end
end
MF.evt2dump_dir = evt2dump_dir

